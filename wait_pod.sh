#!/usr/bin/env bash
while [[ $(kubectl get pods $1 -n $2 -o 'jsonpath={..status.conditions[?(@.type=="Ready")].status}') != "True" ]]; do echo "waiting for pod..." && sleep 1; done
