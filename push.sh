#!/usr/bin/env bash
START_PWD=`pwd`
cd ../../
MAIN_PWD=`pwd`
cd "$START_PWD"
for d in `find .. -name ".git"`; do
    cd "$d/.."
    GITLAB_PROJECT_PATH=$(realpath --relative-to="$MAIN_PWD" "$PWD")
    echo "GITLAB_PROJECT_PATH=$GITLAB_PROJECT_PATH"
    rm -rf .git
    git init
    git config user.name "Administrator"
    git config http.sslVerify false
    git remote add origin "http://oauth2:$GITLAB_TOKEN@gitlab.gitlab.$IP.nip.io/$GITLAB_PROJECT_PATH.git"
    git config --get remote.origin.url
    git add .
    git commit -m "😍 Initial commit"
    git push -u origin --all
    git push -u origin --tags
    cd "$START_PWD"
    echo
done
