#!/usr/bin/env bash
external_ip=""
i=0
while [ -z $external_ip ]; do
  ((i=i+1))
  echo "$i: Waiting for end point..."
  external_ip=$(kubectl get -n ingress svc $1 --template="{{range .status.loadBalancer.ingress}}{{.ip}}{{end}}")
  [ -z "$external_ip" ] && sleep 6
done
echo 'End point ready:' && echo $external_ip
