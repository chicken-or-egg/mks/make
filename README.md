# $ make all

## System dependencies
- [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/)
- [opencstack](https://docs.openstack.org/newton/user-guide/common/cli-install-openstack-command-line-clients.html)
- [helm3](https://helm.sh/docs/intro/install/)
- terraform
- Makefile
- python3 (bs4, lxml, requests)


## Clean
```bash
$ make clean
```



## Kubernetes

```bash
$ make k8s
$ make kubeconfig
```

## Gitlab

```bash
$ make gitlab
$ make push
$ make runner
```

## Ingress

```bash
$ make ing
$ make 
```