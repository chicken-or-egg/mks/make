#!/usr/bin/python3
"""
Script that creates Personal Access Token for Gitlab API;
Tested with GitLab Community Edition 12.6.4
"""
import sys
import requests
from urllib.parse import urljoin
from bs4 import BeautifulSoup
import datetime
import os

verify_flag = sys.argv[4] if len(sys.argv) > 4 else False
endpoint = sys.argv[3] if len(sys.argv) > 3 else  "https://gitlab.com/"
root_route = urljoin(endpoint, "/")
sign_in_route = urljoin(endpoint, "/users/sign_in")
pat_route = urljoin(endpoint, "/profile/personal_access_tokens")

login = sys.argv[1] if len(sys.argv) > 1 else "root"
password = sys.argv[2] if len(sys.argv) > 2 else os.getenv("GITLAB_PASSWORD")


def find_csrf_token(text):
    soup = BeautifulSoup(text, "lxml")
    token = soup.find(attrs={"name": "csrf-token"})
    param = soup.find(attrs={"name": "csrf-param"})
    data = {param.get("content"): token.get("content")}
    return data


def obtain_csrf_token():
    r = requests.get(root_route, verify=verify_flag)
    token = find_csrf_token(r.text)
    return token, r.cookies


def sign_in(csrf, cookies):
    data = {
        "user[login]": login,
        "user[password]": password,
        "user[remember_me]": 0,
        "utf8": "✓"
    }
    data.update(csrf)
    r = requests.post(sign_in_route, data=data, cookies=cookies, verify=verify_flag)
    token = find_csrf_token(r.text)
    return token, r.history[0].cookies


def obtain_personal_access_token(name, expires_at, csrf, cookies):
    data = {
        "personal_access_token[expires_at]": expires_at,
        "personal_access_token[name]": name,
        "personal_access_token[scopes][]": "api",
        "utf8": "✓"
    }
    data.update(csrf)
    r = requests.post(pat_route, data=data, cookies=cookies, verify=verify_flag)
    soup = BeautifulSoup(r.text, "lxml")
    token = soup.find('input', id='created-personal-access-token').get('value')
    return token


def main():
    csrf1, cookies1 = obtain_csrf_token()
#     print("root", csrf1, cookies1)
    csrf2, cookies2 = sign_in(csrf1, cookies1)
#     print("sign_in", csrf2, cookies2)
    tomorrow = datetime.date.today() + datetime.timedelta(days=1)
    name = 'iac-'+ tomorrow.strftime('%Y-%m-%d')
    expires_at = tomorrow.strftime('%Y-%m-%d')
    token = obtain_personal_access_token(name, expires_at, csrf2, cookies2)
    print(token)


if __name__ == "__main__":
    main()
