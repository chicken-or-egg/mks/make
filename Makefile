#!make

# Fake aim
.PHONY: k8s kubeconfig ingress


# Kubernetes
#MY_SELECTEL_TOKEN = token
K8S_REPO = ../internal/K8S-cluster/terraform
K8S_YML = ../internal/K8S-cluster/yaml
K8S_YML_FILES = $(wildcard $(K8S_YML)/*.yml)
K8S_PLANFILE = $(K8S_REPO)/planfile
K8S_OUTPUT = $(K8S_REPO)/.output
FLOATING_IP = $(K8S_REPO)/.ip
K8S_TERRAFORM_INIT = $(K8S_REPO)/.terraform/init
K8S_ID = $(K8S_REPO)/.k8s_id
K8S_REGION=ru-3
MKS_API_IP=https://$(K8S_REGION).mks.selcloud.ru
K8S_USER = $(shell whoami)
K8S_READY = $(K8S_REPO)/.ready




# Openstack
OPENSTACK_TOKEN = $(K8S_REPO)/.os_token
OS_USERNAME = $(K8S_REPO)/.username
OS_PASSWORD = $(K8S_REPO)/.password
OS_PROJECT_ID = $(K8S_REPO)/.project_id

OS_REGION_NAME=$(K8S_REGION)
OS_USER_DOMAIN_NAME = $(shell echo $(MY_SELECTEL_TOKEN) | cut -d_ -f2)




# INGRESS
INGRESS_REPO = ../internal/K8S-cluster/ingress
INGRESS_HELM_VALUES = $(INGRESS_REPO)/values.yml
INGRESS_DEPLOY =  $(INGRESS_REPO)/.status
EXTERNAL_IP = $(INGRESS_REPO)/.ip



# Cert-Manager
CERT_REPO = ../internal/K8S-cluster/cert-manager
CERT_DEPLOY = $(CERT_REPO)/.status

CERT_ISSUER = $(CERT_REPO)/letsencrypt-prod.yaml
CERT_HELM_VALUES = $(CERT_REPO)/values.yml



# Gitlab
GITLAB_REPO = ../internal/gitlab/gitlab
GITLAB_TOKEN = $(GITLAB_REPO)/.token
GITLAB_PASSWORD = $(GITLAB_REPO)/.password
GITLAB_DEPLOY = $(GITLAB_REPO)/.status
GITLAB_PODS = $(GITLAB_REPO)/.pods
GITLAB_HELM_VALUES = $(GITLAB_REPO)/values.yml
GITLAB_READY = $(GITLAB_REPO)/.ready


# Hierarchy
HIERARCHY_REPO = ../internal/gitlab/hierarchy
HIERARCHY_INIT = $(HIERARCHY_REPO)/.terraform/init
HIERARCHY_DEPLOY = $(HIERARCHY_REPO)/.status
HIERARCHY_PLANFILE = $(HIERARCHY_REPO)/planfile


# GLOBAL EXPORT
.EXPORT_ALL_VARIABLES:
# Kubernetes
KUBECONFIG=$(K8S_REPO)/kubeConfig.yaml


# Kubernetes
$(K8S_TERRAFORM_INIT):
	@echo Starting kubernetes deploy
	cd $(K8S_REPO) && terraform init && terraform validate
	touch $(K8S_TERRAFORM_INIT)

$(K8S_PLANFILE): $(K8S_TERRAFORM_INIT)
	cd $(K8S_REPO) && \
	env \
    TF_VAR_region=$(K8S_REGION) \
    TF_VAR_username=$(K8S_USER) \
    TF_VAR_my_selectel_token=$(MY_SELECTEL_TOKEN) \
	terraform plan -out $(shell basename $(K8S_PLANFILE))

$(K8S_OUTPUT): $(K8S_PLANFILE)
	cd $(K8S_REPO) && \
	terraform apply -input=false -auto-approve $(shell basename $(K8S_PLANFILE)) && \
	terraform output > $(shell basename $(K8S_OUTPUT))

printenv:
	echo $(MY_SELECTEL_TOKEN)


# Kubernetes->Output
$(OS_USERNAME): $(K8S_OUTPUT)
	cd $(K8S_REPO) && terraform output user_name > $(shell basename $(OS_USERNAME))

$(OS_PASSWORD): $(K8S_OUTPUT)
	cd $(K8S_REPO) && terraform output user_pass > $(shell basename $(OS_PASSWORD))

$(OS_PROJECT_ID): $(K8S_OUTPUT)
	cd $(K8S_REPO) && terraform output project_id > $(shell basename $(OS_PROJECT_ID))

$(K8S_ID): $(K8S_OUTPUT)
	cd $(K8S_REPO) && terraform output k8s_id > $(shell basename $(K8S_ID))



# Get OPENSTACK_TOKEN
$(OPENSTACK_TOKEN): $(OS_USERNAME)  $(OS_PASSWORD)  $(OS_PROJECT_ID)
	export OS_USERNAME=$(shell cat $(OS_USERNAME)) && \
    export OS_PASSWORD=$(shell cat $(OS_PASSWORD)) && \
    export OS_PROJECT_ID=$(shell cat $(OS_PROJECT_ID)) && \
    export OS_TENANT_ID=$(shell cat $(OS_PROJECT_ID)) && \
    export OS_USER_DOMAIN_NAME=$(OS_USER_DOMAIN_NAME) && \
    export OS_REGION_NAME=$(OS_REGION_NAME) && \
    export OS_VOLUME_API_VERSION=3 && \
    export OS_IDENTITY_API_VERSION=3 && \
    export OS_AUTH_URL="https://api.selvpc.ru/identity/v3" && \
    export CLIFF_FIT_WIDTH=1 && \
	openstack token issue -c id -f value > $(OPENSTACK_TOKEN)



# Get Kubeconfig
$(KUBECONFIG): $(OPENSTACK_TOKEN) $(K8S_ID)
	curl -XGET -H "x-auth-token: $(shell cat $(OPENSTACK_TOKEN))" "$(MKS_API_IP)/v1/kubeversions"
	curl -XGET -H "x-auth-token: $(shell cat $(OPENSTACK_TOKEN))" "$(MKS_API_IP)/v1/clusters"
	curl -XGET -H "x-auth-token: $(shell cat $(OPENSTACK_TOKEN))" \
	"$(MKS_API_IP)/v1/clusters/$(shell cat $(K8S_ID))/kubeconfig" -o $(KUBECONFIG)


# FAKE
kubeconfig: $(KUBECONFIG)
	@echo KUBECONFIG is ready
	@echo "export KUBECONFIG=$(shell pwd)/$(KUBECONFIG)"


# Prepare kubernetes
$(K8S_READY): $(KUBECONFIG)
	$(foreach var,$(K8S_YML_FILES),kubectl apply -f $(var);)
	echo K8S is ready > $(K8S_READY)


k8s-yml:
	$(foreach var,$(K8S_YML_FILES),kubectl apply -f $(var);)


# STEP 1.
# FAKE
k8s: $(K8S_READY)
	@echo K8S is ready!



k8s-destroy:
	@echo TERRAFORM DESTROY
	cd $(K8S_REPO) && \
	env \
    TF_VAR_region=$(K8S_REGION) \
    TF_VAR_username=$(K8S_USER) \
    TF_VAR_my_selectel_token=$(MY_SELECTEL_TOKEN) \
    terraform destroy -input=false -auto-approve


k8s-clean:
	@echo Clean var files
	rm -f $(K8S_OUTPUT)
	rm -f $(K8S_READY)
	rm -f $(K8S_PLANFILE)
	rm -f $(OS_USERNAME)
	rm -f $(OS_PASSWORD)
	rm -f $(OS_PROJECT_ID)
	rm -f $(KUBECONFIG)
	rm -f $(OPENSTACK_TOKEN)
	rm -f $(K8S_ID)
	rm -rf $(K8S_REPO)/.terraform
	rm -f $(K8S_REPO)/*tfstate*



# Install nginx-ingress
$(INGRESS_DEPLOY): $(K8S_READY)
	helm repo add nginx-stable https://helm.nginx.com/stable
	sleep 2
	helm upgrade nginx-ingress nginx-stable/nginx-ingress -n ingress --install -f $(INGRESS_HELM_VALUES)
	helm status nginx-ingress -n ingress -o json | jq .info.status | tr -d \" > $(INGRESS_DEPLOY)


$(EXTERNAL_IP): $(INGRESS_DEPLOY)
	@echo I am waiting external ip
	./wait_ingress.sh nginx-ingress-nginx-ingress
	kubectl get svc -n ingress nginx-ingress-nginx-ingress -o jsonpath="{.status.loadBalancer.ingress[0].ip}" > $(EXTERNAL_IP)


# STEP 2.

ingress: $(EXTERNAL_IP)
	curl -I $(shell cat $(EXTERNAL_IP)).nip.io


ing-clean:
	rm -f $(CERT_DEPLOY)
	rm -f $(INGRESS_DEPLOY)
	rm -f $(EXTERNAL_IP)

# cert-manager
#$(CERT_DEPLOY): $(EXTERNAL_IP)
#	@echo Installing cert-manager
#	helm repo add jetstack https://charts.jetstack.io
#	helm upgrade cert-manager jetstack/cert-manager -n cert-manager --wait --atomic --install -f $(CERT_HELM_VALUES)
#	helm status cert-manager -n cert-manager -o json | jq .info.status | tr -d \" > $(CERT_DEPLOY)


# STEP 3.
#cert-issuer: $(CERT_DEPLOY)
#	kubectl apply -f $(CERT_ISSUER)








# Gitlab
$(GITLAB_DEPLOY): $(EXTERNAL_IP)
	helm repo add gitlab https://charts.gitlab.io
	helm upgrade gitlab gitlab/gitlab -n gitlab  --install -f $(GITLAB_HELM_VALUES) \
	--set "global.hosts.domain=gitlab.$(shell cat $(EXTERNAL_IP)).nip.io"
	helm status -n gitlab -o json | jq .info.status | tr -d \" > $(GITLAB_DEPLOY)



$(GITLAB_PASSWORD): $(GITLAB_READY) $(KUBECONFIG)
	kubectl get secret -n gitlab gitlab-gitlab-initial-root-password -o jsonpath='{.data.password}' | base64 --decode > $(GITLAB_PASSWORD)


$(GITLAB_TOKEN): $(GITLAB_PASSWORD)
	python3 get_gitlab_token.py root $(shell cat $(GITLAB_PASSWORD)) http://gitlab.gitlab.$(shell cat $(EXTERNAL_IP)).nip.io > $(GITLAB_TOKEN)



# STEP 4.
$(GITLAB_READY): $(GITLAB_PODS) $(KUBECONFIG)
	kubectl get po -n gitlab
	@echo Waiting gitlab
	./wait_gitlab.sh $(GITLAB_PODS)
	echo Gitlab is ready > $(GITLAB_READY)

$(GITLAB_PODS): $(GITLAB_DEPLOY)
	kubectl get po -n gitlab | grep gitlab-web | awk '{print $$1}' > $(GITLAB_PODS)



# Fake
gitlab:
	@cat $(GITLAB_READY)



gitlab-clean:
	rm -f $(GITLAB_DEPLOY)
	rm -f $(GITLAB_PASSWORD)
	rm -f $(GITLAB_TOKEN)
	rm -f $(GITLAB_PODS)
	rm -f $(GITLAB_READY)




# Hierarchy
$(HIERARCHY_INIT): $(GITLAB_TOKEN)
	cd $(HIERARCHY_REPO) && terraform init && terraform validate
	touch $(HIERARCHY_INIT)


$(HIERARCHY_PLANFILE): $(HIERARCHY_INIT)
	cd $(HIERARCHY_REPO) && \
	env \
    TF_VAR_gitlab_base_url=http://gitlab.gitlab.$(shell cat $(EXTERNAL_IP)).nip.io \
    TF_VAR_gitlab_token=$(shell cat $(GITLAB_TOKEN)) \
    terraform plan -out $(shell basename $(HIERARCHY_PLANFILE))


# STEP 5.
$(HIERARCHY_DEPLOY): $(HIERARCHY_PLANFILE)
	cd $(HIERARCHY_REPO) && terraform apply -input=false -auto-approve $(shell basename $(HIERARCHY_PLANFILE))
	touch $(HIERARCHY_DEPLOY)


hierarchy-clean:
	rm -f $(HIERARCHY_DEPLOY)
	rm -f $(HIERARCHY_INIT)
	rm -f $(HIERARCHY_PLANFILE)
	rm -rf $(HIERARCHY_REPO)/.terraform
	rm -f $(HIERARCHY_REPO)/*tfstate*








# STEP 6.
push: $(HIERARCHY_DEPLOY)
	export GITLAB_TOKEN=$(shell cat $(GITLAB_TOKEN)) && \
	export IP=$(shell cat $(EXTERNAL_IP)) && \
	./push.sh


# Default
all: push
	@echo SUCCESS!



# STEP 0.
clean: k8s-destroy k8s-clean ing-clean gitlab-clean hierarchy-clean
	@echo Mr. Cleaner was here
